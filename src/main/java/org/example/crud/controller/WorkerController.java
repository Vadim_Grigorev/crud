package org.example.crud.controller;

import org.example.crud.data.Address;
import org.example.crud.data.dto.PhoneNumberDto;
import org.example.crud.data.dto.WorkerDto;
import org.example.crud.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "", produces = "application/json")
public class WorkerController {

  @Autowired
  private WorkerService workerService;

  @PostMapping("/worker/create")
  public void create(@RequestBody @Validated WorkerDto workerDto) {
    workerService.add(workerDto);
  }

  @DeleteMapping("/worker/delete/{id}")
  public void delete(@PathVariable Long id) {
    workerService.delete(id);
  }

  @PostMapping("/worker/archive/{id}")
  public void archive(@PathVariable Long id) {
    workerService.archive(id);
  }

  @PostMapping("/worker/activate/{id}")
  public void activate(@PathVariable Long id) {
    workerService.activate(id);
  }

  @PostMapping("/worker/rename/{id}")
  public void rename(@PathVariable Long id, @RequestBody String name) {
    workerService.renameById(id, name);
  }

  @PostMapping("/worker/changeAddress/{id}")
  public void changeAddress(@PathVariable Long id, @RequestBody @Validated Address address) {
    workerService.changeAddress(id, address);
  }

  @PostMapping("/phone/add/{workerId}")
  public void addPhone(@PathVariable Long workerId, @RequestBody PhoneNumberDto phoneNumberDto) {
    workerService.addPhone(workerId, phoneNumberDto);
  }

  @DeleteMapping("/phone/delete/{workerId}")
  public void deletePhone(@PathVariable Long workerId, @RequestBody PhoneNumberDto phoneNumberDto) {
    workerService.deletePhone(workerId, phoneNumberDto);
  }

}
