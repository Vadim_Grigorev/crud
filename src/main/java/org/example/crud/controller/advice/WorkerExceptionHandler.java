package org.example.crud.controller.advice;

import org.example.crud.controller.advice.exception.NumberCanNotBeDeletedException;
import org.example.crud.controller.advice.exception.PhoneNumberNotFoundException;
import org.example.crud.controller.advice.exception.WorkerDoesNotHaveSuchPhoneNumberException;
import org.example.crud.controller.advice.exception.WorkerIsAlreadyActiveException;
import org.example.crud.controller.advice.exception.WorkerIsAlreadyDisabledException;
import org.example.crud.controller.advice.exception.WorkerNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class WorkerExceptionHandler {

  @ExceptionHandler(PhoneNumberNotFoundException.class)
  public ResponseEntity<String> handlePhoneNumberNotFoundException() {
    return new ResponseEntity<>("Phone number with such id does not exist", HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(WorkerNotFoundException.class)
  public ResponseEntity<String> handleWorkerNotFoundException() {
    return new ResponseEntity<>("Worker with such id does not exist", HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(WorkerIsAlreadyDisabledException.class)
  public ResponseEntity<String> handleWorkerIsAlreadyDisabledException() {
    return new ResponseEntity<>("This worker is already disabled", HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(WorkerIsAlreadyActiveException.class)
  public ResponseEntity<String> handleWorkerIsAlreadyActiveException() {
    return new ResponseEntity<>("This worker is already active", HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(NumberCanNotBeDeletedException.class)
  public ResponseEntity<String> handleNumberCanNotBeDeletedException() {
    return new ResponseEntity<>("Worker must have at least one phone number", HttpStatus.BAD_REQUEST);
  }

  @ExceptionHandler(WorkerDoesNotHaveSuchPhoneNumberException.class)
  public ResponseEntity<String> handleWorkerDoesNotHaveSuchPhoneNumberException() {
    return new ResponseEntity<>("This worker has no such phone number", HttpStatus.BAD_REQUEST);
  }

}
