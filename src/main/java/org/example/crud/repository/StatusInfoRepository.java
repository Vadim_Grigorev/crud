package org.example.crud.repository;

import org.example.crud.data.StatusInfo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusInfoRepository extends JpaRepository<StatusInfo, Long> {

}
