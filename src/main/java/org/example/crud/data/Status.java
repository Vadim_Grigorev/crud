package org.example.crud.data;

public enum Status {
  ACTIVE, DISABLED
}
