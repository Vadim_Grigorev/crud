package org.example.crud.data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PhoneNumber {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String countryCode;

  private String cityCode;

  private String phoneNumber;

  public PhoneNumber() {
  }

  public Long getId() {
    return id;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public String getCityCode() {
    return cityCode;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setCountryCode(String countryCode) {
    this.countryCode = countryCode;
  }

  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

}
