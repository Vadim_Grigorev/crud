package org.example.crud.data;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.example.crud.controller.advice.exception.NumberCanNotBeDeletedException;

@Entity
public class Worker {

  @Id
  @GeneratedValue
  private Long id;

  @NotBlank
  private String name;

  @NotBlank
  private String surname;

  private String secondName;

  @Embedded
  private Address address;

  @OneToMany
  @JoinColumn(name = "worker_id")
  private List<StatusInfo> statuses = new ArrayList<>();

  @Size(min = 1, message = "У работника должен быть указан хотябы 1 телефон")
  @Valid
  @OneToMany
  @JoinColumn(name = "worker_id")
  private List<PhoneNumber> phoneNumbers = new ArrayList<>();

  private LocalDate created;

  private LocalDate updated;

  public Worker() {

  }

  public void setName(String name) {
    this.name = name;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public void setSecondName(String secondName) {
    this.secondName = secondName;
  }


  public void setStatuses(List<StatusInfo> statuses) {
    this.statuses = statuses;
  }

  public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
  }

  public Long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getSecondName() {
    return secondName;
  }

  public void setAddress(Address address) {
    this.address = address;
  }

  public Address getAddress() {
    return address;
  }

  public List<StatusInfo> getStatuses() {
    return statuses;
  }

  public List<PhoneNumber> getPhoneNumbers() {
    return phoneNumbers;
  }

  public LocalDate getCreated() {
    return created;
  }

  public LocalDate getUpdated() {
    return updated;
  }

  @PrePersist
  public void onCreate() {
    created = LocalDate.now();
  }

  @PreUpdate
  public void onUpdate() {
    updated = LocalDate.now();
  }

  public void addNumber(PhoneNumber phoneNumber) {
    phoneNumbers.add(phoneNumber);
  }

  public void deleteNumber(PhoneNumber phoneNumber) {
    if (phoneNumbers.size() == 1) {
      throw new NumberCanNotBeDeletedException();
    }
    phoneNumbers.remove(phoneNumber);
  }

  public void addStatus(StatusInfo statusInfo) {
    statuses.add(statusInfo);
  }
}

