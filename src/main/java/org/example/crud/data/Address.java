package org.example.crud.data;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Embeddable
public class Address {

  @NotBlank
  private String country;

  private String region;

  @NotBlank
  private String city;

  private String street;

  private String house;

  public Address() {
  }

  public Address(String country, String region, String city, String street, String house) {
    this.country = country;
    this.region = region;
    this.city = city;
    this.street = street;
    this.house = house;
  }

  public String getCountry() {
    return country;
  }

  public String getRegion() {
    return region;
  }

  public String getCity() {
    return city;
  }

  public String getStreet() {
    return street;
  }

  public String getHouse() {
    return house;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public void setRegion(String region) {
    this.region = region;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public void setStreet(String street) {
    this.street = street;
  }

  public void setHouse(String house) {
    this.house = house;
  }

}
