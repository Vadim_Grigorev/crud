package org.example.crud.data.dto;

public class PhoneNumberDto {

  private final String countryCode;

  private final String cityCode;

  private final String phoneNumber;

  public PhoneNumberDto(String countryCode, String cityCode, String phoneNumber) {
    this.countryCode = countryCode;
    this.cityCode = cityCode;
    this.phoneNumber = phoneNumber;
  }

  public String getCountryCode() {
    return countryCode;
  }

  public String getCityCode() {
    return cityCode;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }
}
