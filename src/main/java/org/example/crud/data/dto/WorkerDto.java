package org.example.crud.data.dto;

import org.example.crud.data.Address;
import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

public class WorkerDto {

  @NotBlank(message = "Не указано имя работника")
  private final String name;

  @NotBlank(message = "Не указана фамиля работника")
  private final String surname;

  private final String secondName;

  private final Address address;

  @NotEmpty(message = "У работника должен быть указан хотябы один телефон")
  private final List<PhoneNumberDto> phoneNumbers;

  public WorkerDto(String name, String surname, String secondName, Address address,
      List<PhoneNumberDto> phoneNumbers) {
    this.name = name;
    this.surname = surname;
    this.secondName = secondName;
    this.address = address;
    this.phoneNumbers = phoneNumbers;
  }

  public String getName() {
    return name;
  }

  public String getSurname() {
    return surname;
  }

  public String getSecondName() {
    return secondName;
  }

  public Address getAddress() {
    return address;
  }

  public List<PhoneNumberDto> getPhoneNumbers() {
    return phoneNumbers;
  }

}


