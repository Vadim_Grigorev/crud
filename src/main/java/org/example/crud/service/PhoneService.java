package org.example.crud.service;

import org.example.crud.controller.advice.exception.PhoneNumberNotFoundException;
import org.example.crud.data.PhoneNumber;
import org.example.crud.data.dto.PhoneNumberDto;
import org.example.crud.repository.PhoneNumberRepository;
import org.springframework.stereotype.Service;

@Service
public class PhoneService {

  private final PhoneNumberRepository numberRepository;

  public PhoneService(PhoneNumberRepository numberRepository) {
    this.numberRepository = numberRepository;
  }

  public PhoneNumber findById(Long id) {
    return numberRepository.findById(id).orElseThrow(PhoneNumberNotFoundException::new);
  }

  public void create(PhoneNumber phoneNumber) {
    numberRepository.save(phoneNumber);
  }

  public void delete(PhoneNumber phoneNumber) {
    numberRepository.delete(phoneNumber);
  }

  public PhoneNumber mapDto(PhoneNumberDto phoneNumberDto) {
    PhoneNumber phoneNumber = new PhoneNumber();

    phoneNumber.setCountryCode(phoneNumberDto.getCountryCode());
    phoneNumber.setCityCode(phoneNumberDto.getCityCode());
    phoneNumber.setPhoneNumber(phoneNumberDto.getPhoneNumber());

    return phoneNumber;
  }
}
