package org.example.crud.service;

import org.example.crud.data.StatusInfo;
import org.example.crud.repository.StatusInfoRepository;
import org.springframework.stereotype.Service;

@Service
public class StatusInfoService {
  private final StatusInfoRepository statusInfoRepository;

  public StatusInfoService(StatusInfoRepository statusInfoRepository) {
    this.statusInfoRepository = statusInfoRepository;
  }

  public void create(StatusInfo statusInfo) {
    statusInfoRepository.save(statusInfo);
  }

  public void delete(StatusInfo statusInfo) {
    statusInfoRepository.delete(statusInfo);
  }

}
