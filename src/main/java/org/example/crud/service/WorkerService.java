package org.example.crud.service;

import org.example.crud.controller.advice.exception.WorkerDoesNotHaveSuchPhoneNumberException;
import org.example.crud.controller.advice.exception.WorkerIsAlreadyActiveException;
import org.example.crud.controller.advice.exception.WorkerIsAlreadyDisabledException;
import org.example.crud.controller.advice.exception.WorkerNotFoundException;
import org.example.crud.data.Address;
import org.example.crud.data.PhoneNumber;
import org.example.crud.data.Status;
import org.example.crud.data.StatusInfo;
import org.example.crud.data.Worker;
import org.example.crud.data.dto.PhoneNumberDto;
import org.example.crud.data.dto.WorkerDto;
import org.example.crud.repository.WorkerRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

@Service
public class WorkerService {

  private final WorkerRepository workerRepository;

  private final PhoneService phoneService;

  private final StatusInfoService statusInfoService;

  public WorkerService(WorkerRepository workerRepository, PhoneService phoneService, StatusInfoService statusInfoService) {
    this.workerRepository = workerRepository;
    this.phoneService = phoneService;
    this.statusInfoService = statusInfoService;
  }

  public void add(WorkerDto workerDto) {

    Worker worker = mapDto(workerDto);

    worker.getStatuses().forEach(statusInfoService::create);
    worker.getPhoneNumbers().forEach(phoneService::create);

    workerRepository.save(worker);
  }

  public void delete(Long id) {

    Worker worker = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);

    worker.getPhoneNumbers().forEach(phoneService::delete);
    worker.getStatuses().forEach(statusInfoService::delete);

    workerRepository.delete(worker);
  }

  public void archive(Long id) {

    Worker worker = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);

    if (worker.getStatuses().get(worker.getStatuses().size() - 1).getStatus() == Status.DISABLED) {
      throw new WorkerIsAlreadyDisabledException();
    }

    StatusInfo statusInfo = new StatusInfo();

    statusInfo.setStatus(Status.DISABLED);

    statusInfoService.create(statusInfo);

    worker.addStatus(statusInfo);
    workerRepository.save(worker);
  }

  public void activate(Long id) {

    Worker worker = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);

    if (worker.getStatuses().get(worker.getStatuses().size() - 1).getStatus() == Status.ACTIVE) {
      throw new WorkerIsAlreadyActiveException();
    }

    StatusInfo statusInfo = new StatusInfo();

    statusInfo.setStatus(Status.ACTIVE);
    statusInfoService.create(statusInfo);

    worker.addStatus(statusInfo);
    workerRepository.save(worker);
  }

  public void renameById(Long id, String name) {

    Worker worker = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);

    worker.setName(name);
    workerRepository.save(worker);
  }

  public void changeAddress(Long id, Address address) {

    Worker worker = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);

    worker.setAddress(address);
    workerRepository.save(worker);
  }

  public void addPhone(Long id, PhoneNumberDto phoneNumberDto) {

    Worker worker = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);

    createAndSetPhone(worker, phoneNumberDto);

    workerRepository.save(worker);
  }

  public void deletePhone(Long id, PhoneNumberDto phoneNumberDto) {

    Worker worker = workerRepository.findById(id).orElseThrow(WorkerNotFoundException::new);

    for (PhoneNumber number : worker.getPhoneNumbers()) {
      if (phoneNumberDto.getCityCode().equals(number.getCityCode()) &&
          phoneNumberDto.getCountryCode().equals(number.getCountryCode()) &&
          phoneNumberDto.getPhoneNumber().equals(number.getPhoneNumber())
      ) {
        worker.deleteNumber(number);
        phoneService.delete(number);
        workerRepository.save(worker);

        return;
      }
    }

    throw new WorkerDoesNotHaveSuchPhoneNumberException();
  }

  private Worker mapDto(WorkerDto workerDto) {

    Worker worker = new Worker();

    worker.setName(workerDto.getName());
    worker.setSurname(workerDto.getSurname());
    worker.setSecondName(workerDto.getSecondName());
    worker.setAddress(workerDto.getAddress());

    for (PhoneNumberDto phoneNumberDto : workerDto.getPhoneNumbers()) {
      PhoneNumber phoneNumber = phoneService.mapDto(phoneNumberDto);
      worker.addNumber(phoneNumber);
    }

    worker.setStatuses(new ArrayList<>(List.of(new StatusInfo())));

    return worker;
  }

  private void createAndSetPhone(Worker worker, PhoneNumberDto phoneNumberDto) {
    PhoneNumber phoneNumber = phoneService.mapDto(phoneNumberDto);

    phoneService.create(phoneNumber);
    worker.addNumber(phoneNumber);
  }
}
